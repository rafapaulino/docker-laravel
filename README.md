
Publicando Laravel com o Docker - Code Education

[ATUALIZAÇÃO]

Agora que você já aprendeu muito sobre docker, gostaríamos que dividisse esse exercício em duas etapas:

1) Configurar um ambiente Laravel utilizando o docker-compose com:

Nginx
PHP-FPM
Redis
MySQL
Lembrando que o volume do código fonte deve ser compartilhado com a App.

Após realizarmos a clonagem do repositório e executarmos: docker-compose up -d, poderemos ver a aplicação Laravel rodando com o erro de autoload na porta: 8000, uma vez que o docker-compose não executou o composer install do PHP, logo, não se preocupe com tal detalhe nesse momento. 

2) Após ter tido sucesso na etapa anterior, faça a configuração do framework Laravel seguindo as etapas (dentro do container):

execute composer install
crie o arquivo .env baseado no .env.example 
execute: php artisan key:generate 
execute: php artisan migrate
* Nesse momento, quando você acessar a aplicação no browser, finalmente, você deverá ver a página inicial do Laravel funcionando.

Baseado nessas últimas ações, gere o build da imagem desse container e faça a publicação em sua conta no Hub do Docker.

Lembre-se: Ao gerar o build da imagem, TODO o conteúdo incluindo arquivos como vendor, .env, etc deverão ser incluídos.

Adicione o endereço da imagem no seu dockerhub no README.md e faça o commit para um repositório público do Github.

Arquivos e códigos úteis para auxiliar no exercício incluindo nginx.conf e linha de comando para baixar o composer. Clique aqui.

-------------------------------------------------------------------------------------------------------------------


Para executar o docker e criar os containers é só rodar:
docker-compose up -d 

Para parar a execução:
docker-compose down

-------------------------------------------------------------------------------------------------------------------

Comando úteis:

docker rum hello-world

docker ps - containers rodando
docker ps -a mostra todos os containers que já rodaram

docker rm iddocontainer - remove o container

docker images - lista as imagens

docker pull nginx
docker run nginx

docker run -d nginx - roda o container em background

docker stop iddocontainer - para o processo

docker em iddocontainer -f remove o container mesmo se ele estiver ativo e rodando

docker run nginx:alpine

Baixa a imagem rodando em background e colocando um nome nela 

docker run -d --name my_nginx nginx:alpine
docker ps

docker stop my_nginx
docker start my_nginx


comando completo expondo portas (porta do micro/container)
docker run -d --name nginx_com_porta -p 8080:80 nginx:alpine

docker run -d --name nginx_com_porta2 -p 80:80 nginx:alpine



Executar comandos no container que esta rodando

o container tem uma layer onde vc pode mexer

docker ps

executa comandos no container
docker exec nginx_com_porta2 uname -a

docker run -d --name nginx_default -p 80:80 nginx

interagir com o bash
docker exec -it nginx_default bash 





Executar comandos no container que esta rodando

o container tem uma layer onde vc pode mexer

docker ps

executa comandos no container
docker exec nginx_com_porta2 uname -a

docker run -d --name nginx_default -p 80:80 nginx

interagir com o bash
docker exec -it nginx_default bash 


Trabalhando com volumes
docker stop nginx_default
docker rm nginx_default - remove o container

Comando -v pastanocomputador:pastanocontainer

docker run -d --name nginx_default -p 80:80 -v $(pwd):/usr/share/nginx/html nginx

Entrar na máquina e verificar o diretorio do nginx
docker exec -it nginx_default bash 
cd /usr/share/nginx/html
ls -la




Criando volumes


docker volume ls

docker volume create vol_test

docker volume inspect vol_test

criando um volume com as opções

docker volume create --driver local --opt type=none --opt device=$(pwd) --opt o=bind volume_local

docker volume inspect volume_local

usando o volume

docker run -d --name=nginx2 -p 8081:80 -v volume_local:/usr/share/nginx/html nginx

docker ps

docker volume prune (mata todos os volumes)


Trabalhando com networks

docker network ls

criar 2 containers:

docker run -d --name=nginx1 nginx
docker run -d --name=nginx3 nginx
docker ps

docker exec -it nginx1 bash
apt-get update
apt-get install iputils-ping

ping nginx1

docker network inspect bridge

docker network create -d bridge my_network 
docker network ls

docker run -d --name=nginx4 --net=my_network nginx
docker run -d --name=nginx5 --net=my_network nginx

docker exec -it nginx4 bash
apt-get update
apt-get install iputils-ping
ping nginx5







Docker commit

docker ps -a (mostra todos os containers)

docker ps -a -q (mostra so os hashs dos containers)

remove todos os containers

docker rm $(docker ps -a -q)

para todos os containers
docker stop $(docker ps -q)

docker run -d --name=nginx -p 8080:80 nginx

docker ps

fazer alteração no container e gerar uma imagem a partir dessa alteração:

docker exec -it nginx bash

cd /usr/share/nginx/html

apt-get update
apt-get install vim
vim index.html (editar o arquivo)
exit

docker ps (pegar o id do container)

docker commit 1082b55140e5 rafa/nginx-commit
docker images (lista as imagens)

criar um novo container a partir dessa imagem

docker run -d --name=nginx1 -p 8081:80 rafa/nginx-commit

docker exec -it nginx1 bash
cd /usr/share/nginx/html






enviando a imagem para o dockerhub

docker images

docker login (loga no dockerhub)

docker push rafapaulinodev/nginx-commit:latest

docker tag <old_name> <new_name>
$ docker rmi <old_name>


trabalhando com o docker file

docker build -t test_swoole .

colocando a imagem para rodar

docker run -d --name swoole -p 9501:9501 test_swoole

subindo a imagem para o dockerhub

docker build -t rafapaulinodev/php-swoole:latest .

docker push rafapaulinodev/php-swoole:latest

ver o index.php e o dockerfile








Instalando laravel com dockerfile

composer create-project --prefer-dist laravel/laravel laravel

docker build -t rafapaulinodev/laraveldocker:latest .

docker run -d --name laravel -v $(pwd):/var/www/ -p 8000:8000 rafapaulinodev/laraveldocker

instalando o bash no container

docker exec -it laravel apk add bash

Entrar no bash
docker exec -it laravel bash

cd /var/www
ls 

iniciar o servidor do laravel
php artisan serve --host=0.0.0.0

dockerfile:
FROM php:7.4.0-fpm-buster
EXPOSE 9000
ENTRYPOINT ["php-fpm"]




Utilizando o Xdebug com o vscode e phpstorm
https://imasters.com.br/devsecops/como-usar-o-xdebug-dentro-de-um-container-docker
https://www.pascallandau.com/blog/setup-phpstorm-with-xdebug-on-docker/
https://jansenfelipe.com.br/2019/09/20/debugando-uma-aplicacao-php-no-vscode-com-xdebug-docker/
https://phpdocker.io/generator


Docker compose

docker-compose up -d
docker-compose down
docker-compose up -d --build (força o docker a recriar a maquina)


dockerize
https://github.com/jwilder/dockerize



Endereço da imagem:

docker push rafapaulinodev/laravel-schoolofnet:latest


Endereço do docker compose:
https://github.com/GoogleCloudPlatform/cloud-builders-community

